---
title: "pubs"
date: 2023-06-19
draft: false
---

Checkout [google scholar](https://scholar.google.com/citations?user=jxreoWYAAAAJ&hl=en&oi=ao) for more.

- JJ Blackner, DM Rooney, JW Hollett and JA McCubbin [Ferrocenium Boronic Acid Catalyzed Deoxygenative Coupling of Alcohols with Carbon- and Nitrogen-Based Borate and Silane Nucleophiles](https://pubs.acs.org/doi/10.1021/acs.joc.3c00463), *J Org Chem*, **2023**, *ASAP*

- I Elayan, R Gupta and JW Hollett, [\\(\Delta\\)NO and the complexities of electron correlation in simple hydrogen clusters](https://doi.org/10.1063/5.0073227), *J Chem Phys*, **2022**, *156*, 094102

- HR Bloomfield, JW Hollett and JS Ritch, [Crystal structure and com­putational study of an oxo-bridged bis-titanium(III) com­plex](https://doi.org/10.1107/S2053229621006094), *Acta Cryst C*, **2021**, *77*, 391

- LG Todd and JW Hollett, [Measuring correlated electron motion in atoms with the momentum-balance density](https://doi.org/10.1063/5.0039387), *J Chem Phys*, **2021**, *154*, 074110

- JW Hollett and P-F Loos, [Capturing static and dynamic correlation with \\(\Delta\\)NO-MP2 and \\(\Delta\\)NO-CCSD](https://doi.org/10.1063/1.5140669), *J Chem Phys*, **2020**, *152*, 014101 

- I Elayan, M Almatarneh and JW Hollett, [The bimolecular catalytic transformation of methyl vinyl ketone oxide: A DFT study](https://doi.org/10.1016/j.chemphys.2019.110649), *Chem Phys*, **2020**, *530*, 110649 

- M Almatarneh, I Elayan, M Altarawneh and JW Hollett, [Computational study of the ozonolysis of sabinene](https://link.springer.com/article/10.1007/s00214-019-2420-7), *Theor Chem Acc*, **2019**, *138*, 30

- M Almatarneh, I Elayan, M Altarawneh and JW Hollett, [Reactivity of the anti-Criegee intermediate of \\(\beta\\)-pinene with prevalent atmospheric species](https://link.springer.com/article/10.1007/s11224-019-1288-4), *Struct Chem*, **2019**, *30*, 1353

- I Elayan, M Almatarneh and JW Hollett, [Reactivity of the anti-Criegee intermediate of \\(\beta\\)-pinene with prevalent atmospheric species](https://link.springer.com/article/10.1007/s11224-019-1288-4), *Struct Chem*, **2019**, *30*, 1353

- JW Hollett and N Pegoretti, [On-top density functionals for the short-range dynamic correlation between electrons of opposite and parallel-spin](https://aip.scitation.org/doi/10.1063/1.5025171), *J Chem Phys*, **2018**, *148*, 164111

- M Almatarneh, I Elayan, M Altarawneh and JW Hollett, [Hydration and Secondary Ozonide of the Criegee Intermediate of Sabinene](https://pubs.acs.org/doi/abs/10.1021/acsomega.7b02002), *ACS Omega*, **2018**, *3*, 2417

- D Fuss, YQ Wu, MR Grossi, JW Hollett and TE Wood, [Effect of the tether length upon Truce-Smiles rearrangement reactions](http://onlinelibrary.wiley.com/doi/10.1002/poc.3742/full), *J Phys Org Chem*, **2018**, *31*, e3742

- JW Hollett and W Li, [The relative alignment of electron momenta in atoms and molecules and the effect of a static electric field](http://pubs.acs.org/doi/full/10.1021/acs.jpca.7b09439), *J Phys Chem A*, **2017**, *121*, 8026

- P Sarte, A Aczel, G Ehlers, C Stock, B Gaulin, C Mauws, M Stone, S Calder, S Nagler, JW Hollett, H Zhou, J Gardner, J Attfield and C Wiebe, [Evidence for the confinement of magnetic monopoles in quantum spin ice](https://doi.org/10.1088/1361-648X/aa8ec2), *J Phys Condens Matter*, **2017**, 29, 45LT01

- JW Hollett, H Hosseini, and C Menzies, [A cumulant functional for static and dynamic correlation](http://scitation.aip.org/content/aip/journal/jcp/145/8/10.1063/1.4961243), *J Chem Phys*, **2016**, *145*, 084106

