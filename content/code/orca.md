---
title: "ORCA"
draft: false
weight: 11
---

ORCA is an ab initio, DFT, and semi-empirical SCF-MO package. Find it [here](https://orcaforum.kofo.mpg.de/).
