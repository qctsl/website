---
title: "IQmol"
draft: false
weight: 12
---

IQmol is a free open-source molecular editor and visualization package. It offers a range of features including a molecular editor, surface generation (orbitals and densities) and animations (vibrational modes and reaction pathways). Find it [here](http://iqmol.org/)
