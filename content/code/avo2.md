---
title: "Avogadro 2"
draft: false
weight: 12
---

Avogadro 2 is a chemical editor and visualization application, it is also a set of reusable software libraries written in C++ using principles of modularity for maximum reuse. Find it [here](https://www.openchemistry.org/projects/avogadro2/)
