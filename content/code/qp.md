---
title: "Quantum Package"
draft: false
weight: 2
---

Quantum Package is an open-source programming environment for quantum chemistry specially designed for wave function methods. Its main goal is the development of determinant-driven selected configuration interaction (sCI) methods and multi-reference second-order perturbation theory (PT2). Find it [here](https://quantumpackage.github.io/qp2/).
