---
title: "AMPED"
draft: false
weight: 1
---

Object-oriented electronic structure theory code dedicated to *Alternative Models and Properties of the two-Electron Density* (A fork of MUNgauss). Find it on [gitlab](https://gitlab.com/qctsl/amped).
