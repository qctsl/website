---
title: "Edwin Somy joins the QCTSL"
date: 2023-05-11
draft: false
---

Edwin joins the group as a Mitacs Globalink Research Intern for 12 weeks. His research will involve using machine learning to quickly compute two-electron density functional energies.
