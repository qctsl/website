---
title: "Gurleen Cheema transfers to PhD"
date: 2023-05-01
draft: false
---

Gurleen has transferred from MSc to PhD.  Her work will extend from correlated electron motion in the excited state to developing new two-electron density functionals.

