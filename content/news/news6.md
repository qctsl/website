---
title: "Ernesto Cruz Velázquez joins the QCTSL"
date: 2024-09-03
draft: false
---

Ernesto joins the group as an MSc student. Ernesto recently earned his MSc from National Autonomous University of Mexico (UNAM). His initial research will focus on the use of machine-learning models to accelerate the computation of correlation energies from two-electron density functionals.
