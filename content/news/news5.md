---
title: "Sakshi Dakhra joins the QCTSL"
date: 2024-05-06
draft: false
---

Sakshi joins the group as a University of Winnipeg Undergraduate Summer Research Assistant International (USRA-I) [Congratulations!]. Her research will involve the identification of important interactions with and design of new inhibitors of a cancer target, and an investigation of the relative angular momentum of electrons in atoms and small molecules.
