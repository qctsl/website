---
title: "Vedant Nimbhorkar joins the QCTSL"
date: 2023-05-06
draft: false
---

Vedant joins the group as a Mitacs Globalink Research Intern for 12 weeks. His research will focus on the analysis of chemical bonding using momentum-balance density.
