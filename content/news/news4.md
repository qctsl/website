---
title: "Igal Press joins the QCTSL"
date: 2024-04-11
draft: false
---

Igal joins the group as an undergraduate research assistant for about 4 weeks. His research will involve an investigation of nonadiabatic effects in small molecules made from exotic particles.
