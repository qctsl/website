---
title: "Kiera Augusto"
draft: false
summary: "Undergraduate researcher (May 2018 - August 2019)"
weight: 154
---
**Undergraduate researcher (May 2018 - August 2019)**
***
## project
Drug resistance of NS5B and alchemical free energy simulations.
***
**current position**: PhD student (Joseph Thwyissen, Toronto)
