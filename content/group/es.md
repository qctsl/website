---
title: "Edwin Somy"
draft: false
summary: Mitacs Globalink Research Intern (May 2023 - July 2023)
weight: 5
---
**Mitacs Globalink Research Intern (May 2023 - July 2023)**
***
## project
Quick computation of two-electron density functional energies using machine learning.
***
