---
title: "Ismael Elayan"
draft: false
summary: MSc student (May 2018 - December 2021)
weight: 151
---
**MSc student (May 2018 - December 2021)**
***
## project
Tackling the static correlation challenge with the \\(\Delta\\)NO
method.
***
**current position**: PhD student (Alex Brown, Alberta)

