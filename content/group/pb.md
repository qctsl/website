---
title: "Paras Boruah"
draft: false
summary: "Mitacs Globalink Research Intern (May 2022 - July 2022)"
weight: 7
---
**Mitacs Globalink Research Intern (May 2022 - July 2022)**
***
## project
Analysis of two-electron and on-top density functionals for universal
correlation.
***
**current position**: PhD student (Michigan)
