---
title: "Ernesto Cruz Velázquez"
draft: false
summary: MSc student (September 2024 - present)
weight: 1
---
**MSc student (September 2024 - present)**
***
[comment]: #![GKC](img/gkc.png)
***
- **office**: 3RC049
## project
The use of machine-learning models to accelerate the computation of correlation energies from two-electron density functionals.

