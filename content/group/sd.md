---
title: "Sakshi Dakhra"
draft: false
summary: Undergraduate Summer Research Assistant - International (May 2024 - August 2024)
weight: 4
---
**Undergraduate Summer Research Assistant - International (May 2024 - August 2024)**
***
## project
Investigation of virtual screening results for the design of new inhibitors of an important cancer target.
***
