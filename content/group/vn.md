---
title: "Vedant Nimbhorkar"
draft: false
summary: Mitacs Globalink Research Intern (May 2023 - July 2023)
weight: 6
---
**Mitacs Globalink Research Intern (May 2023 - July 2023)**
***
## project
Analysis of chemical bonding using momentum-balance density.
***

