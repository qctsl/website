---
title: "Gurleen Cheema"
draft: false
summary: PhD student (May 2022 - present)
weight: 2
---
**Undergraduate student (May 2021 - April 2022)**\
**PhD student (May 2022 - present)**
***
![GKC](img/gkc.png)
***
- **office**: 3RC049
## project
The correlated motion of electrons in excited states and the development of new two-electron density functionals based on relative angular momentum and virial compliance.

