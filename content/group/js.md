---
title: "Jatinder Singh"
draft: false
summary: "Undergraduate researcher (May 2017 - August 2018)"
weight: 180
---
**Undergraduate researcher (May 2017 - August 2018)**
***
## project
Computational study of aza-Piancatelli reaction.
***
**current position**: Technician (Manitoba Hydro)
