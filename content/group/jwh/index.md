---
title: "Josh Hollett"
draft: false
summary: "group leader"
weight: 3
---
**group leader**
***
![JWH](img/jwh.png)
***
- **office**: 2RC019
- **email**: j(dot)hollett(at)uwinnipeg(dot)ca
