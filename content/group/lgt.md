---
title: "Lucy Todd"
draft: false
summary: "Honours student (June 2019 - August 2019)"
weight: 152
---
**Honours student (June 2019 - August 2019)**
***
## project
Analysis of atomic momentum-balance densities.
***
**current position**: PhD student (University College London)
