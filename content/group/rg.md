---
title: "Rishabh Gupta"
draft: false
summary: "Mitacs Globalink Research Intern (May 2018 - July 2018)"
weight: 154
---
**Mitacs Globalink Research Intern (May 2018 - July 2018)**
***
## project
A quadratically convergent orbital optimization algorithm for the \\(\Delta\\)NO
method.
***
**current position**: PhD student (Purdue)
