---
title: "qctsl blog"
draft: false
---

A former student once suggested that I start a blog, describing, explaining, and discussing theoretical chemistry.  I think the idea was to present some of the theories and ideas at a more approachable level.  What follows (eventually) is my attempt to follow through on that suggestion (thanks Ismael) while lightly inserting my opinion on some topics at the same time.  Posts to come soon.
