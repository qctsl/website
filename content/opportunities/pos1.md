---
title: "PhD positions available"
draft: true
---

The qctsl is currently searching for multiple PhD (or MSc) students to perform theoretical chemistry research, particularly in the area of electronic structure theory development.  Potential projects include: the development of two-electron density functionals, machine-learning for density functionals, new approaches to modelling multireference systems (particularly excited states), the extension of the \\(\Delta\\)NO method to periodic systems, analysis of superconductivity with quantum two-particle properties, and other possibilities.

**funding**: $27k/year (minimum including TA-ship)\
**start date**: January 2024 or May 2024\
**note**: Positions are through the [University of Manitoba Chemistry Department](https://umanitoba.ca/explore/programs-of-study/chemistry-phd)

Programming skills are an asset but not required.  Curiosity and enthusiasm are required, and the rest can be learned during study. Please contact [me](../../group/jwh/) if you are interested.
