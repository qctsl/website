---
title: "CHEM-1111"
draft: false
---
## Introduction the chemical properties of matter

This course includes an introduction to atomic and molecular structure, chemical bonding, chemical reactivity, to the bulk properties of matter, and the descriptive chemistry of the elements. The laboratory component introduces students to basic chemistry laboratory practice and techniques. The
fundamental concepts of chemical reactivity covered in this course and CHEM-1112 provide the essential foundation for students who wish to continue with Chemistry or Biochemistry as a major, and for students of Biology, Physics, Physical Geography, Environmental Studies, and Experimental Psychology.

[Recent course outline](doc/CHEM1111_Outline.pdf)
