---
title: "CHEM-2103"
draft: false
---
## Atoms, molecules and spectroscopy

This course is an introduction to quantum chemistry, with applications in atomic and molecular structure and spectroscopy. Laboratory work consists of experiments in molecular spectroscopy and computational methods for determining molecular structures and properties.

[Recent course outline](doc/CHEM2103_Outline.pdf)
