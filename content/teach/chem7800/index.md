---
title: "CHEM-7800"
draft: false
---
## Electronic Structure Theory

Computational chemistry methods have become invaluable tools for research in most all areas of chemistry. The theories and algorithms behind these methods are firmly rooted in quantum mechanics, linear algebra, numerical mathematics, and computer science. This course covers the foundations of both wave function and density functional methods, and the algorithms used to implement these approaches in modern computational chemistry software. It also introduces a selection of advances to these methods, that involve the incorporation of novel approximations or algorithms, which are of current interest to theoretical chemistry research.

[Recent course outline](doc/CHEM7800_Outline.pdf)
