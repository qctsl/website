---
title: "CHEM-4101"
draft: false
---
## Quantum chemistry

This course covers the fundamentals of quantum chemistry, with an introduction to the electronic structure
theory of molecules. The course also introduces computational chemistry techniques as valuable tools for
research in all fields of chemistry.

[Recent course outline](doc/CHEM4101_Outline.pdf)
