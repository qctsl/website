---
title: "CHEM-2701"
draft: false
---
## Computer techniques and applications in chemistry

This course introduces students to the use of computer software for the collection, manipulation and analysis of data in a chemistry setting. Topics include data handling; basic statistical analysis; graphing; computational modelling, and the incorporation and presentation of data in scientific documents.

[Recent course outline](doc/CHEM2701_Outline.pdf)
