---
title: "CHEM-2104"
draft: false
---
## Computational drug design

Computational drug design is a scientific research field that encompasses aspects of physical chemistry,
organic chemistry, biochemistry, molecular biology, and computer science. This course explores aspects of
computational drug design from a physical chemistry perspective, including crystallography, molecular modelling, ligand-protein docking, and machine-learning in chemistry. A particular focus is placed on the computational design of drugs for viral protein targets, such as human immunodeficiency virus (HIV), hepatitis C virus (HCV), and severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2).

[Recent course outline](doc/CHEM2104_Outline.pdf)
