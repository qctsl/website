---
title: "two-particle properties"
draft: false
summary: The derivation, implementation, measurement and analysis of new properties of quantum mechanical particles to further our understanding of the many-electron wave function. read more...
weight: 2
---
The many-electron wave function is a $(3N+1)$-dimensional object which, when added to the wave-particle duality of electrons, makes it a difficult thing to picture, let alone understand.  However, considering that the hamiltonian of the Schr&ouml;dinger equation contains only interactions of two-electrons at a time (i.e. Coulomb repulsion) then most of the necessary understanding can be extracted from the two-electron density matrix,
{{< katex >}}
\begin{align}
\Gamma(\bm{r}_1,\bm{r}_2,\bm{r}_1',\bm{r}_2') = \frac{N(N-1)}{2} \int & \Psi^*(\bm{r}_1',\bm{r}_2',\bm{r}_3,\dots,\bm{r}_N) \nonumber \\ \times & \Psi(\bm{r}_1,\bm{r}_2,\bm{r}_3,\dots,\bm{r}_N) d\bm{r}_3 \dots d\bm{r}_N .
\end{align}
{{< /katex >}}
All two-electron properties can be calculated with the 2-RDM and the diagonal of the 2-RDM is the two-electron density, {{< katex >}} $\Gamma(\bm{r}_1,\bm{r}_2) = \Gamma(\bm{r}_1,\bm{r}_2,\bm{r}_1,\bm{r}_2)$ {{< /katex >}}.
![2ed](img/2ed.png "Exact (alpha-beta) two-electron density of H4")
One can directly analyze the two-electron density by fixing the position of an electron in an atom or molecule to discover how electrons position themselves relative to each other (see above).  However, particularly because electrons are quantum particles, positions do not always provide a picture that is clear enough.  Therefore, in order to clarify the picture and provide an alternative perspective we derive 
new two-electron properties in both position and momentum-space.  One such property is momentum-balance,
{{< katex >}}
\begin{equation}
\lambda^\pm = \langle \Phi| \delta\left(\bm{p}_1 \mp \bm{p}_2\right) |\Phi \rangle = \frac{1}{(2\pi)^3} \int \Gamma(\bm{r}_1,\bm{r}_2,\bm{r}_1+\bm{q},\bm{r}_2 \mp \bm{q})  d\bm{q} d\bm{r}_1 d\bm{r}_2,
\end{equation}
{{< /katex >}}
where $\Phi$ is the momentum-space wave function. Momentum-balance is the difference in probability that electron pairs will have the exact same or exact opposite momentum.  For electrons (or any particles) with uncorrelated motion the momentum-balance is *zero*.  Therefore, momentum-balance can serve as a [correlated motion *detector*](https://doi.org/10.1063/5.0039387).  We have also discovered that momentum-balance is also a good indicator of potential dependent correlation and could be used as an ingredient in automated active space selection for multireference wave function calculations.

In an effort to improve our description of [universal correlation, using two-electron density functionals,](../cft) we have derived an additional two-electron property, the relative angular momentum (RAM).
![RAM](img/RAMdiag.png "Relative angular momentum of two electrons")
The behaviour of electrons at short separation distances (*i.e.*, shape of the cusp) is highly dependent on the RAM of the two electrons.  Our measure gives us the ability to calculate this property at a given point in space and subsequently incorporate it into a two-electron density functional.

Current research is focused on the analysis of momentum-balance in chemical bonding and non-covalent interactions, the use of momentum-balance for active space selection, analysing the role of RAM in the correlated motion of electrons in atoms and molecules, and incorporating RAM into two-electron density functionals.
