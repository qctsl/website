---
title: "computational chemistry"
draft: false
summary: "Using our own models and existing state-of-the-art computational methodology to study problems in organometallics, organocatalysis and molecular virology, in collaboration with experimental researchers."
weight: 3
---

Usually in collaboration with researchers at the University of Winnipeg, in both [chemistry](https://www.uwinnipeg.ca/chemistry/) and [biology](https://www.uwinnipeg.ca/chemistry/), we use computational methods to study a wide range of problems.  Some examples include: analysis of the low-lying electronic states of dititanium organometallic complexes, investigation of the role of stereochemistry in the complexation of analytes with the fluorescent probe europium(III)-tetracycline, computational design of boronic acid based organocatalysts, and predicting drug resistant mutations in the hepatitis C viral protein NS5B using free-energy-perturbation methods.

