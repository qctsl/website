---
title: "cumulant functional theory"
draft: false
summary: "The development, implementation and refinement of an electronic structure model based on the approximation of the two-electron density matrix. read more..."
weight: 1
---

Our electronic structure method development is set in the framework of cumulant functional theory (CFT). Cumulant functional theory involves approximating the two-electron density matrix (2-RDM), \\( \Gamma \\), in terms of the one-electron density matrix (1-RDM).  The first two terms of the expansion, \\( \Gamma_\text{0-1RDM} \\), roughly correspond to the Coulomb and exchange repulsion terms of Hartree-Fock theory.  The remaining, unknown, part of the two-electron density matrix is the cumulant, \\( \Gamma_q \\),
$$ \Gamma = \Gamma_\text{0-1RDM} + \Gamma_q $$
Similar to density functional theory (DFT), where the universal functional is unknown, the cumulant of CFT is also unknown.  Most approximations of the cumulant involve using a post-HF-like approach with perturbation theory, or coupled-cluster-like expansions.  We believe the most economical approach is to treat different forms of electron correlation (which is what the cumulant describes) with their own approximation.  This leads to the partitioning of the cumulant (and hence electron correlation) into *potential dependent* and *universal* components,
$$ \Gamma_q = \Gamma_\text{pot} + \Gamma_\text{uni}. $$
Potential dependent correlation is the necessary for correct description of bond dissociation, charge transfer, and the angular correlation that occurs around atomic nuclei.  Universal correlation is necessary for the correct description of the short to medium range correlated motion of electrons, including the electron-electron cusp, which is present with or without nuclei.

The most effective way to deal with potential dependent correlation is through expansion in terms of molecular orbitals,

$$ \Gamma_\text{pot}(\bm{r}\_1,\bm{r}\_2,\bm{r}\_1\',\bm{r}\_2\') = \sum\_{abcd} \Gamma^\text{pot}\_{abcd} \phi\_a^{\*}(\bm{r}\_1\') \phi\_b^{\*}(\bm{r}\_2\')\phi\_c(\bm{r}\_1)\phi\_d(\bm{r}\_2),  $$

given that molecular orbitals implicitly and explicitly contain the necessary information (*i.e.*, location of nuclei and nodes).  Our [\\(\Delta\\)NO method](https://doi.org/10.1063/5.0073227) is an orbital expansion method that begins with a relatively simple wave function and augments the resulting 2-RDM with additional corrections.

Universal correlation can be effectively modelled with a functional of the two-electron density (*i.e.*, a two-electron density functional). Essentially, this means modifying a two-electron density without universal correlation, \\( \Gamma^0(\bm{r}\_1,\bm{r}\_2) \\), such that it exhibits the proper (exact) behaviour when electrons interact (*e.g.*, the cusp),
$$ \Gamma(\bm{r}\_1,\bm{r}\_2) = \Gamma^0(\bm{r}\_1,\bm{r}\_2)\big[ 1 - g(u,\bm{R}) \left(1 -\Phi(\bm{R}) c(u)  \right) \big], $$
where \\( \bm{u} = \bm{r}_1 - \bm{r}_2 \\) and \\( \bm{R} = \frac{\bm{r}_1 + \bm{r}_2}{2} \\).  Typically, the proper behaviour, \\(c(u)\\),  is inserted over some range of \\(u\\) that is determined by \\(g(u,\bm{R})\\).  The universal correlation energy is equal to the change in repulsion energy due to the modification of the two-electron density.

In practice, we combine \\(\Delta\\)NO with a two-electron density functional (TDF), or a more approximate on-top density functional (ODF).  Much of our current research is concerned with improving the corrections applied to the \\(\Delta\\)NO 2-RDM, extending \\(\Delta\\)NO to electronic excited states, the derivation of highly accurate two-electron density functionals and devising algorithms for their rapid evaluation.  The goal is to develop an efficient and accurate electronic structure method by using the most *natural* model for the interactions of many electrons.
